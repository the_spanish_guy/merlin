$(document).ready(function() {

	//	Lightbox pure JavaScript
	var boxCon = document.getElementsByClassName("boxConteudo");	
	var fecha = document.getElementsByClassName("fa-times");	

	function idProduto(i) {
		switch (i) {
		    case "potion01":
		        return 1
		        break;
		    case "potion02":
		        return 2
		        break;
		    case "potion03":
		        return 3
		        break;
		    case "potion04":
		        return 4
		        break;
		    case "potion05":
		        return 5
		        break;
		    case "potion06":
		        return 6
		        break;		    
		}
	}

	function lightboxConstr(array,index) {
		var nomepocao = document.getElementById('nomepocao');
		var efeito = document.getElementById('efeito');
		var ingredientes = document.getElementById('ingredientes');
		var preco = document.getElementById('preco');
		var potionsArray = array[index];

		nomepocao.innerHTML = potionsArray.name;
		efeito.innerHTML = potionsArray.effect;
		preco.innerHTML = "$" + potionsArray.price;		

		var output = "";		
		for (var i = 0; i < potionsArray.ingredients.length; i++) {
			output += "<li>" + potionsArray.ingredients[i] + "</li>";
		}
		
		ingredientes.innerHTML = output;			
	}

	function getFromJson(index) {
		var myRequest = new XMLHttpRequest();
		var url = "assets/potions.json";
		myRequest.open("GET", url);
		myRequest.onload = function () {
			var myData = (JSON.parse(myRequest.responseText)).potions;			
			lightboxConstr(myData,index);
		}; 
		myRequest.send();
	}		

	function Open() {				
		productId = this.parentNode.id; // store the id of the figure's parent  (Ex.:potion01,potion02...)
		index = idProduto(productId);		
		getFromJson(index);							
		lightBoxBg.style.display = "flex"; // how to display after de content is build?
	}

	function Close() {
		lightBoxBg.style.display = "none";
	}

	for (var i = 0; i < boxCon.length; i++) {
		boxCon[i].addEventListener("click",Open);
	}
	fecha[0].addEventListener("click",Close);


});